const {expect, assert} = require("chai")
const {getCircleArea, checkIfPassed, getAverage, getSum, getDifference, factorial, divCheck} = require("../util.js");

describe("Test get are of a circle", () => {
	it("Test area of circle radius 15 is 706.86", () =>{
		let area = getCircleArea(15);
		assert.equal(area, 706.86)
	});

	it("Test area of circle with radius of 9 is 254.47", () =>{
		let area = getCircleArea(9);
		expect(area).to.equal(254.46959999999999);
	})
})

describe("Test check if passed", () => {
	it("Test 25 out of 30 is passed", () => {
		let isPassed = checkIfPassed(25, 30)
		assert.equal(isPassed, true);
	});
	it("Test if 30 out of 50 is not passed", () => {
		let isPassed = checkIfPassed(30,50);
		assert.equal(isPassed, false);
	})
})

describe("Test average of four numbers", () => {
	it("Test if average of 80,82,84 and 86 is 83", () => {
		let avg = getAverage(80, 82, 84, 86)
		expect(avg).is.equal(83);
	})

	it("Test if average of 70,80,82 and 84 is 79", () => {
		let avg = getAverage(70, 80, 82, 84)
		expect(avg).is.equal(79);
	})
})

describe("Test get sum", () => {
	it("Test sum of 15 and 30 is 45", () => {
		let sum = getSum(15, 30);
		assert.equal(sum,45);
	})
	it("Test sum of 25 and 50 is 75", () => {
		let sum = getSum(25, 50);
		assert.equal(sum,75);
	})
})

describe("Test get difference", () => {
	it("Test difference 70 and 40 is 30", () => {
		let diff = getDifference(70, 40);
		assert(diff ,30);
	})
	it("Test difference 125 and 50 is 75", () => {
		let diff = getDifference(125, 50);
		assert(diff, 75);
	})	
})

describe("Test factorial", () => {
	it("Test that 5! is 120", () => {
		let num = factorial(5);
		expect(num).to.equal(120);
	})
	it("test that 1! is 1", () => {
		const num = factorial(1);
		expect(num).to.equal(1);
	})
	it("Test that 0! is 1", () => {
		const num = factorial(0)
		expect(num).to.equal(1)
	})
	it("Test negative factorial is undefined", () => {
		const num = factorial(-1)
		expect(num).to.equal(undefined)
	})
	it("Test that non-numeric value returns an error", () => {
		const num = factorial("1")
		expect(num).to.equal(undefined)
	})

})

describe("Test divisible by 5 and 7", () => {
	it("Test that 120 is divisible by 5", () => {
		const num = divCheck(120)
		expect(num).to.equal(true);
	})
	it("Test that 14 is divisible by 7", () => {
		const num = divCheck(14)
		expect(num).to.equal(true);
	})
	it("Test that 105 is divisible by 5 or 7", () => {
		const num = divCheck(105) || divCheck(105)
		expect(num).to.equal(true);
	})
	it("Test that 22 should not be divisible by 5 or 7", () => {
		const num = divCheck(22) || divCheck(22)
		expect(num).to.equal(false);
	})
})