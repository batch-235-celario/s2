// Sample functions to test

function getCircleArea(radius){
	return 3.1416 * (radius**2);
}

function checkIfPassed(score, total) {
	return(score / total)*100 >= 75;
}

function getAverage(num1, num2, num3, num4){
	return (num1 + num2 + num3 + num4) / 4;
}

function getSum (num1, num2){
	return num1 + num2;
}

function getDifference (num1, num2){
	return num1 - num2;
}

const factorial = (n) => {
	if(typeof n !== 'number'){
		return undefined;
	} else if(n < 0){
		return undefined;
	} else if(n === 0){
		return 1;
	} else if(n === 1){
		return 1
	} else {
		return n * factorial(n-1);
	}
}

//Activity
const divCheck = (num) =>{
	if(num % 5 === 0 || num % 7 === 0){
		return true;
	};
		return false;

}

module.exports = {
	getCircleArea: getCircleArea,
	checkIfPassed: checkIfPassed,
	getAverage: getAverage,
	getSum: getSum,
	getDifference: getDifference,
	factorial: factorial,
	divCheck: divCheck
}